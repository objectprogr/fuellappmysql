package pl.devpl.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Fuel {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	@NotNull
//	@Min(1)
	private Long id;
	@NotNull
	@Size(min=9, max=10)
	private String date;
	@NotNull
	@DecimalMin("1.0")
	private float price;
	@NotNull
	@DecimalMin("1.0")
	private float amount;
	
	public Fuel () {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}


	public Fuel(Long id, String date, float price, float amount) {
		super();
		this.id = id;
		this.date = date;
		this.price = price;
		this.amount = amount;

	}

	@Override
	public String toString() {
		return "Fuel [id=" + id + ", date=" + date + ", price=" + price + ", amount=" + amount + "]";
	}


	
}
