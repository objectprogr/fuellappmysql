package pl.devpl.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.devpl.model.Fuel;

@Repository
public interface FuelReposiotry extends JpaRepository<Fuel, Long>{
	Fuel findById(Long id);
	
}
