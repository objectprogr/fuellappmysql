package pl.devpl.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.devpl.model.Fuel;
import pl.devpl.repository.FuelReposiotry;

@Controller
public class FuelController {
	
	private FuelReposiotry fuelReposiotry;
    
	@Autowired
    public FuelController(FuelReposiotry fuelReposiotry) {
        this.fuelReposiotry = fuelReposiotry;
    }
	
	@GetMapping("/")
	public String home(Model model) {
		return "home";
	}
	
	@GetMapping("/add")
	public String addForm(Model model) {
		model.addAttribute("fuel", new Fuel());
		return "add";
	}
	
	@PostMapping("/add")
	public String add(@Valid @ModelAttribute Fuel fuel, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return "add";
		}else {
		fuelReposiotry.save(fuel);
		return "redirect:/";
		}
	}
	@GetMapping("/findAll")
	public String findAll(Model model) {
		List<Fuel> allFuels = fuelReposiotry.findAll();
		model.addAttribute("allFuels", allFuels);
		return "findAll";
	}
	
	@GetMapping("/update")
	public String updateForm(Model model) {
		model.addAttribute("fuel", new Fuel());
		return "update";
	}
	@PostMapping("/update")
	public String update(@Valid @ModelAttribute Fuel fuel,BindingResult bindingResult, Long id) {
		Fuel newFuel = fuelReposiotry.findById(id);		
		newFuel.setDate(fuel.getDate());
		newFuel.setPrice(fuel.getPrice());
		newFuel.setAmount(fuel.getAmount());
		if(bindingResult.hasErrors()) {
			return "update";
		}else {
		fuelReposiotry.save(newFuel);
		return "redirect:/";
		}

	}
}
